var app = angular.module('app',[
    'ui.router',
    'angular-loading-bar',
    'ngCookies',
    'ngFileUpload',
    'timer',
    'ngAnimate',
    'toastr',
    'ngMap',
    'ngResource',
    'ngSanitize',
    'socialbase.sweetAlert',
    'ui.bootstrap'
])

app.config(function($locationProvider) {
	$locationProvider.html5Mode({
		enabled: true
	});

	// timeAgoSettings.allowFuture = true;
});

app.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('base', {
            abstract: true,
            templateUrl: 'views/layout',
            controller: LayoutController
        })
        .state('base.index',{
            url: '/',
            title: 'GoPöol',
            templateUrl: 'views/index',
            controller: IndexController
        })
        .state('base.validEmail',{
            url: '/validEmail',
            title: 'GoPool - Validacion del Correo Electronico',
            templateUrl: 'views/validEmail',
            controller: ValidEmailController
        })
        .state('base.search',{
            url: '/search',
            title: 'GoPool - Busqueda',
            templateUrl: 'views/search',
            controller: SearchController
        })
        .state('base.inventory',{
            url: '/inventory',
            title: 'GoPool - Mis Productos',
            templateUrl: 'views/Inventory',
            controller: InventoryController
        })
        .state('base.poolEnd',{
            url: '/poolEnd',
            title: 'GoPool - Mis Pooles Cerrados',
            templateUrl: 'views/poolEnd',
            controller: PoolEndController
        })
        .state('base.poolEnable',{
            url: '/poolEnable',
            title: 'GoPool - Mis Pooles Abiertos',
            templateUrl: 'views/poolEnable',
            controller: PoolEnableController
        })
        .state('base.poolAsign',{
            url: '/poolAsign',
            title: 'GoPool - Mis Pooles Asignados',
            templateUrl: 'views/poolAsign',
            controller: PoolAsignController
        })
        .state('base.favorites',{
            url: '/favorites',
            title: 'GoPool - Favoritos',
            templateUrl: 'views/favorites',
            controller: FavoritesController
        })
        .state('base.califications', {
            url: '/califications',
            title: 'GoPool - Calificaciones',
            templateUrl: 'views/califications',
            controller: CalificationsController
        })
        .state('base.request',{
            url: '/request',
            title: 'GoPool - Solicitudes',
            templateUrl: 'views/request',
            controller: RequestController
        })
})


