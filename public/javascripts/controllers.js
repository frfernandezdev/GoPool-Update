function LayoutController($scope,$rootScope,$state,$stateParams,$timeout,$interval){
	$rootScope.title = $state.current.title;

	$scope.ToggleSign = function(option){
		if(option){
			$('#SignIn').modal('toggle')
			$timeout(function(){
				$('#SignUp').modal('toggle')
			},500)
		}else {
			$('#SignUp').modal('toggle')
			$timeout(function(){
				$('#SignIn').modal('toggle')
			},500)
		} 
	}

	$scope.uploadFile = function(option){
		if(option)
			$('#File').click();
		else 
			$scope.file = false;
	}

	$scope.uploadPhoto = function(option){
		if(option)
			$('#Photo').click();
		else 
			$scope.photo = false;
	}

	$scope.html = {
		navbar: {
			about: 'Acerca de GoPool',
			termCondition: 'Términos y condiciones - Política de privacidad',
			whatIsIt: '¿Que es?',
			whatNotIsIt: '¿Que no es?',
			cuestionFrecuent: 'Preguntas Frecuentes',
		},
		footer: {
			subcribe: 'Suscribete al Newsletter y recibe alertas de nuevas pooles',
			moreAbout: {
				title: 'Más sobre GoPool',
				p: {
					0: 'Trabaja con nosotros',
					1: 'Términos y Condiciones',
					2: 'Politicas de privacidad ',
					3: 'Ayuda.'
				}
			},			
			supportHelp: {
				title: 'Soporte y Ayuda',
				p: {
					0: 'Iniciar chat asistente',
					1: 'Soporte técnico',
					2: 'Iniciar reclamo',
					3: '(+54) 0810 777 6789',
					4: 'ayuda@gopool.club'
				}
			},
			boxLogin: {
				title: 'Iniciar sesión',
				buttonTitle: 'Entrar',
				checkRemember: 'Recuérdame',
				checkForgotPass: 'Se te olvidó tu contraseña'
			} 

		},
		placeholders: {
			email: 'Ingresá tu email',
			username: 'Nombre de Usuario',
			password: 'Contraseña',

		},
		buttons: {
			SignIn: 'Entrar'
		}
	}

}
function IndexController($scope,$rootScope,$state,$stateParams,$timeout,$interval){
	$rootScope.title = $state.current.title;

	$scope.carousel = {
			0: {
					img: 'http://gpool.pr.wes-it.com:9083/Banner-2-texto.jpg'
			},
			1: {
					img: 'http://gpool.pr.wes-it.com:9083/Banner-1-texto.jpg'
			}
	}

	$scope.categories = {
			0: {
					name: 'Limpieza',
					img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png'
			},
			1: {
					name: 'Limpieza',
					img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png'
			},
			2: {
					name: 'Limpieza',
					img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png'
			},
			3: {
					name: 'Limpieza',
					img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png'
			},
			4: {
					name: 'Limpieza',
					img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png'
			},
			5: {
					name: 'Limpieza',
					img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png'
			},
			6: {
					name: 'Limpieza',
					img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png'
			}
	}

	$scope.poolHighlights = {
			0: {
					name: 'Limpieza',
					img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
					categories: 'Limpieza',
					stock: 50,
					finish: '5 dias',
					price: 50,
					raiting: new Array(1)
			},
			1: {
					name: 'Limpieza',
					img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
					categories: 'Limpieza',
					stock: 50,
					finish: '5 dias',
					price: 50,
					raiting: 1
			},
			2: {
					name: 'Limpieza',
					img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
					categories: 'Limpieza',
					stock: 50,
					finish: '5 dias',
					price: 50,
					raiting:  new Array(5)
			},
			3: {
					name: 'Limpieza',
					img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
					categories: 'Limpieza',
					stock: 50,
					finish: '5 dias',
					price: 50,
					raiting:  new Array(6)
			},
			4: {
					name: 'Limpieza',
					img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
					categories: 'Limpieza',
					stock: 50,
					finish: '5 dias',
					price: 50,
					raiting:  new Array(2)
			},
			5: {
					name: 'Limpieza',
					img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
					categories: 'Limpieza',
					stock: 50,
					finish: '5 dias',
					price: 50,
					raiting:  new Array(4)
			},
			6: {
					name: 'Limpieza',
					img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
					categories: 'Limpieza',
					stock: 50,
					finish: '5 dias',
					price: 50,
					raiting:  new Array(1)
			}
	}

	$scope.html = {
		placeholders: {
			email: 'Ingresá tu email',
			username: 'Nombre de Usuario',
			password: 'Contraseña',
			selectCategories: 'Categorias',
			selectProduct: 'Producto',
			selectZone: 'Zona',
			selectDelivery: 'Delivery',
			selectCantProvee: 'Cantidad de proveedores'
		},
		buttons: {
			search : 'Buscar',
			sales: 'QUIERO VENDER',
			purchase: 'QUIERO COMPRAR',
			btnSale: 'Vender',
			btnPurchase: 'Comprar'
		}
	}

}
function SignInController($scope,$rootScope,$state,$stateParams,$timeout,$interval){
	$rootScope.title = $state.current.title;
}
function SignUpController($scope,$rootScope,$state,$stateParams,$timeout,$interval){
	$rootScope.title = $state.current.title;
}
function ValidEmailController($scope,$rootScope,$state,$stateParams,$timeout,$interval){
	$rootScope.title = $state.current.title;
}
function SearchController($scope,$rootScope,$state,$stateParams,$timeout,$interval){
	$rootScope.title = $state.current.title;

	$scope.poolHighlights = {
		0: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting: new Array(1)
		},
		1: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting: 1
		},
		2: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(5)
		},
		3: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(6)
		},
		4: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(2)
		},
		5: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(4)
		},
		6: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(1)
		}
	}

}
function InventoryController($scope,$rootScope,$state,$stateParams,$timeout,$interval){
	$rootScope.title = $state.current.title;

	$scope.uploadPhoto = function(option){
		if(option)
			$('#Photo').click();
		else 
			$scope.photo = false;
	}

	$scope.file = [0];
	$scope.file[0] = false;
	

	$scope.addFile = function($index){
		$scope.file[$index+1] = false;
	}

	$scope.removeFile = function($index){
		console.log($scope.file)
		if($scope.file.length>1)
			delete $scope.file.splice($index,1);
		else 
			$scope.file[$index] = false;
	}

	$scope.poolHighlights = {
		0: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting: new Array(1)
		},
		1: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting: 1
		},
		2: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(5)
		},
		3: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(6)
		},
		4: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(2)
		},
		5: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(4)
		},
		6: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(1)
		}
	}
}
function PoolEndController($scope,$rootScope,$state,$stateParams,$timeout,$interval){
	$rootScope.title = $state.current.title;

	$scope.poolHighlights = {
		0: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting: new Array(1)
		},
		1: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting: 1
		},
		2: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(5)
		},
		3: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(6)
		},
		4: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(2)
		},
		5: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(4)
		},
		6: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(1)
		}
	}
}
function PoolEnableController($scope,$rootScope,$state,$stateParams,$timeout,$interval){
	$rootScope.title = $state.current.title;

	$scope.poolHighlights = {
		0: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting: new Array(1)
		},
		1: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting: 1
		},
		2: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(5)
		},
		3: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(6)
		},
		4: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(2)
		},
		5: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(4)
		},
		6: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(1)
		}
	}
}
function PoolAsignController($scope,$rootScope,$state,$stateParams,$timeout,$interval){
	$rootScope.title = $state.current.title;

	$scope.poolHighlights = {
		0: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting: new Array(1)
		},
		1: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting: 1
		},
		2: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(5)
		},
		3: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(6)
		},
		4: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(2)
		},
		5: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(4)
		},
		6: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(1)
		}
	}
}
function FavoritesController($scope,$rootScope,$state,$stateParams,$timeout,$interval){
	$rootScope.title = $state.current.title;

	$scope.poolHighlights = {
		0: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting: new Array(1)
		},
		1: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting: 1
		},
		2: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(5)
		},
		3: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(6)
		},
		4: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(2)
		},
		5: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(4)
		},
		6: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(1)
		}
	}
}
function CalificationsController($scope,$rootScope,$state,$stateParams,$timeout,$interval){
	$rootScope.title = $state.current.title;

	$scope.poolHighlights = {
		0: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting: new Array(1)
		},
		1: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting: 1
		},
		2: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(5)
		},
		3: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(6)
		},
		4: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(2)
		},
		5: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(4)
		},
		6: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(1)
		}
	}
}
function RequestController($scope,$rootScope,$state,$stateParams,$timeout,$interval){
	$rootScope.title = $state.current.title;
	
	$scope.poolHighlights = {
		0: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting: new Array(1)
		},
		1: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting: 1
		},
		2: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(5)
		},
		3: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(6)
		},
		4: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(2)
		},
		5: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(4)
		},
		6: {
				name: 'Limpieza',
				img: 'http://gpool.pr.wes-it.com:9083/square/700/1505933101442_0_libreria.png',
				categories: 'Limpieza',
				stock: 50,
				finish: '5 dias',
				price: 50,
				raiting:  new Array(1)
		}
	}
}