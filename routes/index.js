var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/views/:name', function(req, res) {
  res.render('partials/'+req.params.name);
});

router.get('/*', function(req, res) {
  res.render('index');
});

router.get('/backend/:id', function(req, res) {
  res.json(req.param.id);
})

module.exports = router;
